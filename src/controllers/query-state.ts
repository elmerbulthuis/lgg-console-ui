import assert from "assert";
import deepEqual from "fast-deep-equal";
import { InstanceMemoizer } from "instance-memoizer";
import { ReactiveController } from "lit";
import { ComponentBase } from "../components/base.js";
import { QuerySource } from "../utils/query.js";

export class QueryStateController<S, E, A extends unknown[]>
    implements ReactiveController {

    private abortController?: AbortController;
    private queryPromise?: Promise<QuerySource<S, E>>;

    private source?: InstanceMemoizer<Promise<QuerySource<S, E>>, A>;
    private args?: A;
    private linger?: number;

    public state?: S;

    public get loading() {
        return this.state == null;
    }

    constructor(
        private readonly host: ComponentBase,
        private readonly sourceFactory: () => InstanceMemoizer<Promise<QuerySource<S, E>>, A>,
        private readonly argsFactory: () => Readonly<A>,
    ) {
        host.addController(this);
    }

    private async run() {
        assert(this.queryPromise);
        assert(this.abortController);

        const query = await this.queryPromise;

        this.state = query.getState();
        this.host.requestUpdate();
        for await (const { state } of query.fork(this.abortController.signal)) {
            this.state = state;
            this.host.requestUpdate();
        }
    }

    private subscribe(source: InstanceMemoizer<Promise<QuerySource<S, E>>, A>, args: A) {
        if (deepEqual(args, this.args)) {
            return;
        }

        this.unsubscribe();

        const abortController = this.abortController = new AbortController();

        this.queryPromise = source.acquire(...args);
        this.source = source;
        this.args = args;

        this.run().catch(error => {
            if (!abortController.signal.aborted) throw error;
        });
    }

    private unsubscribe() {
        if (this.abortController) {
            this.abortController.abort();
            this.abortController = undefined;
        }

        if (this.queryPromise) {
            assert(this.source != null);

            this.source.release(this.queryPromise, this.linger);
            this.queryPromise = undefined;
        }

        this.source = undefined;
        this.args = undefined;
    }

    hostConnected() {
        const rootComponent = this.host.getRootComponent();
        this.linger = rootComponent.linger;
    }

    hostDisconnected() {
        this.unsubscribe();
    }

    hostUpdate() {
        const source = this.sourceFactory();
        const args = this.argsFactory();

        this.subscribe(source, args);
    }

}
