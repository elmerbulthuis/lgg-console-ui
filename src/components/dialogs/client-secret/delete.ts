import assert from "assert";
import { html } from "lit";
import { property } from "lit/decorators.js";
import { ComponentBase } from "../../base.js";

export class ClientSecretDeleteDialogComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    @property({ type: String, reflect: true })
    digest?: string

    render() {
        const { client } = this;
        assert(client != null, "client not set");

        const { digest } = this;
        assert(digest != null, "digest not set");

        return html`
<h1>
    Delete secret
</h1>

<app-client-secret-delete-form client=${client} digest=${digest}></app-client-secret-delete-form>
`;
    }
}

