import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-client-secret-create-dialog": index.ClientSecretCreateDialogComponent;
        "app-client-secret-delete-dialog": index.ClientSecretDeleteDialogComponent;
    }

}

customElements.define("app-client-secret-create-dialog", index.ClientSecretCreateDialogComponent);
customElements.define("app-client-secret-delete-dialog", index.ClientSecretDeleteDialogComponent);

