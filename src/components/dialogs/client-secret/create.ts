import assert from "assert";
import { html } from "lit";
import { property } from "lit/decorators.js";
import { ComponentBase } from "../../base.js";

export class ClientSecretCreateDialogComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    render() {
        const { client } = this;
        assert(client != null, "client not set");

        return html`
<h1>
    Create secret
</h1>

<app-client-secret-create-form client=${client}></app-client-secret-create-form>
`;
    }
}

