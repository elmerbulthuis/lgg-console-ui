import { html } from "lit";
import { ComponentBase } from "../../base.js";

export class ClientCreateDialogComponent extends ComponentBase {
    render() {
        return html`
<h1>
    Create client
</h1>

<app-client-create-form></app-client-create-form>
`;
    }
}

