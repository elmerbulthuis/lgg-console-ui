import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-client-create-dialog": index.ClientCreateDialogComponent;
        "app-client-delete-dialog": index.ClientDeleteDialogComponent;
        "app-client-update-dialog": index.ClientUpdateDialogComponent;
    }

}

customElements.define("app-client-create-dialog", index.ClientCreateDialogComponent);
customElements.define("app-client-delete-dialog", index.ClientDeleteDialogComponent);
customElements.define("app-client-update-dialog", index.ClientUpdateDialogComponent);

