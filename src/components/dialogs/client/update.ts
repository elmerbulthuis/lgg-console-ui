import assert from "assert";
import { html } from "lit";
import { property } from "lit/decorators.js";
import { ComponentBase } from "../../base.js";

export class ClientUpdateDialogComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    render() {
        const { client } = this;
        assert(client != null, "client not set");

        return html`
<h1>
    Edit client
</h1>

<app-client-update-form client=${client}></app-client-update-form>
`;
    }
}

