import assert from "assert";
import { html } from "lit";
import { property } from "lit/decorators.js";
import { ComponentBase } from "../../base.js";

export class ClientDeleteDialogComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    render() {
        const { client } = this;
        assert(client != null, "client not set");

        return html`
<h1>
    Delete client
</h1>

<app-client-delete-form client=${client}></app-client-delete-form>
`;
    }
}

