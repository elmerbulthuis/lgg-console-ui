import assert from "assert";
import { html } from "lit";
import { property } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";
import { QueryStateController } from "../../../controllers/index.js";
import * as queries from "../../../queries/index.js";
import { ComponentBase } from "../../base.js";

export class ClientSecretListComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    private clientSecretQuery = new QueryStateController(
        this,
        () => this.context.clientSecretQuery,
        () => {
            const { client } = this;
            assert(client != null, "client not set");

            return [client] as const;
        },
    )

    render() {
        const { client } = this;
        assert(client != null, "client not set");

        const clientSecretState = this.clientSecretQuery.state;

        if (!clientSecretState) return html`<p>Loading secrets, please wait...</p>`;

        const list = queries.selectClientSecretList(clientSecretState);
        if (list.length === 0) return html`
<p>
    You have no secrets yet!
</p>
<app-action-bar>
    <app-dialog-action dialogName="client-secret-create" .dialogParameters=${{ client }}><app-icon type="create"></app-icon> Create secret</app-dialog-action>
</app-action-bar>
`;

        return html`
<app-action-bar>
    <app-dialog-action dialogName="client-secret-create"  .dialogParameters=${{ client }}><app-icon type="create"></app-icon> Create secret</app-dialog-action>
</app-action-bar>

<app-scrollable>

<table>
<thead>
    <tr>
        <th></th>
        <th>Created</th>
        <th></th>
    </tr>
</thead>

<tbody>
${repeat(list, item => item.digest, item => html`
<tr>
    <td>${item.digest}</td>
    <td>${item.created}</td>
    <td>
    <app-action-bar inline>
        <app-dialog-action dialogName="client-secret-delete" .dialogParameters=${{ client, digest: item.digest }}>
            <app-icon type="delete"></app-icon>
        </app-dialog-action>
    </app-action-bar>
    </td>
</tr>
`)}
</tbody>
</table>

</app-scrollable>


`;

    }

}

