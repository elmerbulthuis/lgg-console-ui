import assert from "assert";
import { html, nothing } from "lit";
import { property } from "lit/decorators.js";
import { BusyController, QueryStateController } from "../../../controllers/index.js";
import * as queries from "../../../queries/index.js";
import { ComponentBase } from "../../base.js";

export class ClientSecretDeleteFormComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    @property({ type: String, reflect: true })
    digest?: string

    private busy = new BusyController(this);

    private clientSecretQuery = new QueryStateController(
        this,
        () => this.context.clientSecretQuery,
        () => {
            const { client } = this;
            assert(client != null, "client not set");

            return [client] as const;
        },
    )

    render() {
        const { isBusy } = this.busy;

        const { client } = this;
        assert(client != null, "client not set");

        const { digest } = this;
        assert(digest != null, "digest not set");

        const { state } = this.clientSecretQuery;
        if (state == null) return nothing;

        const item = queries.selectClientSecretItem(state, digest);
        if (item == null) return nothing;

        return html`
<p>
    Delete secret?
</p>

<app-action-bar>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleSubmit}>
    <app-icon type="thumb_up"></app-icon>
    Ok
</app-clickable-action>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleCancel}>
    <app-icon type="undo"></app-icon>
    Cancel
</app-clickable-action>
</app-action-bar>
`;
    }

    private handleSubmit = this.busy.wrap(async () => {
        const { client } = this;
        assert(client != null, "client not set");

        const { digest } = this;
        assert(digest != null, "digest not set");

        const result = await this.context.consoleApi.deleteClientSecret({
            parameters: {
                client,
                digest,
            },
        });

        assert(result.status === 204);

        this.context.navigator.back();
    })

    private handleCancel = async (event: Event) => {
        this.context.navigator.back();
    }

}

