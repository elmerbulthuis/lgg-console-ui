import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-client-secret-create-form": index.ClientSecretCreateFormComponent;
        "app-client-secret-delete-form": index.ClientSecretDeleteFormComponent;
        "app-client-secret-list": index.ClientSecretListComponent;
    }

}

customElements.define("app-client-secret-create-form", index.ClientSecretCreateFormComponent);
customElements.define("app-client-secret-delete-form", index.ClientSecretDeleteFormComponent);
customElements.define("app-client-secret-list", index.ClientSecretListComponent);

