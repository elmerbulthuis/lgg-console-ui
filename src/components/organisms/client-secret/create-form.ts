import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import { html } from "lit";
import { property } from "lit/decorators.js";
import { BusyController } from "../../../controllers/index.js";
import { ComponentBase } from "../../base.js";

export class ClientSecretCreateFormComponent extends ComponentBase {
    private busy = new BusyController(this);

    @property({ type: String, reflect: true })
    client?: string

    resultEntity?: consoleOas.CreateClientSecret201ApplicationJsonResponseSchema

    render() {
        const { isBusy } = this.busy;

        if (this.resultEntity == null) {

            return html`
<p>
    Create a new secret?
</p>

<app-action-bar>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleSubmit}>
    <app-icon type="thumb_up"></app-icon>
    Ok
</app-clickable-action>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleCancel}>
    <app-icon type="undo"></app-icon>
    Cancel
</app-clickable-action>
</app-action-bar>
`;
        }

        return html`
<p>
    Your secret is created! This secret is not stored anywhere, and this is the last time you will evefr see it! Please store it somewhere safe...
</p>

<dl>
        <dt>digest</dt>
        <dd>${this.resultEntity.digest}</dd>

        <dt>secret</dt>
        <dd>${this.resultEntity.secret}</dd>
</dl>

<app-action-bar>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleDone}>
    <app-icon type="thumb_up"></app-icon>
    Ok! I'm done!
</app-clickable-action>
</app-action-bar>
`;

    }

    private handleSubmit = this.busy.wrap(async () => {
        const { client } = this;
        assert(client != null, "client not set");

        const result = await this.context.consoleApi.createClientSecret({
            parameters: {
                client,
            },
            entity: () => {
                return {};
            },
        });

        assert(result.status === 201);

        this.resultEntity = await result.entity();
    })

    private handleCancel = async (event: Event) => {
        this.context.navigator.back();
    }

    private handleDone = async (event: Event) => {
        this.context.navigator.back();
    }

}

