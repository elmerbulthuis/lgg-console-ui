import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import { html, nothing, PropertyValues } from "lit";
import { property } from "lit/decorators.js";
import { BusyController, FormController, QueryStateController } from "../../../controllers/index.js";
import * as queries from "../../../queries/index.js";
import { isIterableEmpty } from "../../../utils/iterable.js";
import { FieldValidators } from "../../../utils/validator.js";
import { ComponentBase } from "../../base.js";

type Model =
    consoleOas.UpdateClientApplicationJsonRequestBodySchema

const fieldValidators = {
    ...consoleOas.validateUpdateClientApplicationJsonRequestBodySchema.properties,
} as FieldValidators<Model>;

function hasError(model: Partial<Model>, member: keyof Model): boolean {
    const fieldValidator = fieldValidators[member];
    return !isIterableEmpty(fieldValidator(model[member]));
}

export class ClientUpdateFormComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    private busy = new BusyController(this);

    private clientQuery = new QueryStateController(
        this,
        () => this.context.clientQuery,
        () => [] as const,
    )

    private form = new FormController<Model>(
        this,
        hasError,
    )

    protected getInitialModel() {
        const { client } = this;
        assert(client != null, "client not set");

        const { state } = this.clientQuery;
        if (state == null) return;

        const item = queries.selectClientItem(state, client);
        if (item == null) return;

        return {
            name: item.name,
        };
    }

    render() {
        const { isBusy } = this.busy;
        const { model, errors } = this.form;

        if (model == null || errors == null) return nothing;

        return html`
<fieldset>

<app-text-field
    title="Name"
    data-member="name"
    trim
    required
    .value=${model.name}
    ?error=${errors.name}
></app-text-field>

</fieldset>

<app-action-bar>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleSubmit}>
    <app-icon type="thumb_up"></app-icon>
    Ok
</app-clickable-action>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleCancel}>
    <app-icon type="undo"></app-icon>
    Cancel
</app-clickable-action>
</app-action-bar>
`;
    }

    private handleSubmit = this.busy.wrap(async () => {
        const { model } = this.form;
        assert(model);

        this.form.touch();
        if (!this.form.isValid()) return;

        const { client } = this;
        assert(client, "missing client");

        const result = await this.context.consoleApi.updateClient({
            parameters: {
                client,
            },
            entity: () => {
                return model;
            },
        });

        assert(result.status === 204);

        this.context.navigator.back();
    })

    private handleCancel = async (event: Event) => {
        this.context.navigator.back();
    }

    update(changedProperties: PropertyValues) {
        super.update(changedProperties);

        if (this.form.model != null) return;

        const model = this.getInitialModel();
        if (model == null) return;

        this.form.setModel(model);
    }

}

