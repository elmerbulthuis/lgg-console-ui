import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import { html, nothing, PropertyValues } from "lit";
import { BusyController, FormController } from "../../../controllers/index.js";
import { isIterableEmpty } from "../../../utils/iterable.js";
import { FieldValidators } from "../../../utils/validator.js";
import { ComponentBase } from "../../base.js";

type Model =
    consoleOas.CreateClientApplicationJsonRequestBodySchema

const fieldValidators = {
    ...consoleOas.validateCreateClientApplicationJsonRequestBodySchema.properties,
} as FieldValidators<Model>;

function hasError(model: Partial<Model>, member: keyof Model): boolean {
    const fieldValidator = fieldValidators[member];
    return !isIterableEmpty(fieldValidator(model[member]));
}

export class ClientCreateFormComponent extends ComponentBase {
    private busy = new BusyController(this);

    private form = new FormController<Model>(
        this,
        hasError,
    )

    protected getInitialModel() {
        return {
            name: "",
        };
    }

    render() {
        const { isBusy } = this.busy;
        const { model, errors } = this.form;

        if (model == null || errors == null) return nothing;

        return html`
<fieldset>

<app-text-field
    title="Name"
    data-member="name"
    trim
    required
    .value=${model.name}
    ?error=${errors.name}
></app-text-field>

</fieldset>

<app-action-bar>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleSubmit}>
    <app-icon type="thumb_up"></app-icon>
    Ok
</app-clickable-action>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleCancel}>
    <app-icon type="undo"></app-icon>
    Cancel
</app-clickable-action>
</app-action-bar>
`;
    }

    private handleSubmit = this.busy.wrap(async () => {
        const { model } = this.form;
        assert(model);

        this.form.touch();
        if (!this.form.isValid()) return;

        const result = await this.context.consoleApi.createClient({
            parameters: {},
            entity: () => {
                return model;
            },
        });

        assert(result.status === 201);

        this.context.navigator.back();
    })

    private handleCancel = async (event: Event) => {
        this.context.navigator.back();
    }

    update(changedProperties: PropertyValues) {
        super.update(changedProperties);

        if (this.form.model != null) return;

        const model = this.getInitialModel();
        this.form.setModel(model);
    }

}

