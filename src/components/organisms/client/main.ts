import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-client-create-form": index.ClientCreateFormComponent;
        "app-client-delete-form": index.ClientDeleteFormComponent;
        "app-client-update-form": index.ClientUpdateFormComponent;
        "app-client-list": index.ClientListComponent;
    }

}

customElements.define("app-client-create-form", index.ClientCreateFormComponent);
customElements.define("app-client-delete-form", index.ClientDeleteFormComponent);
customElements.define("app-client-update-form", index.ClientUpdateFormComponent);
customElements.define("app-client-list", index.ClientListComponent);

