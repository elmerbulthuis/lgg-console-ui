import { html } from "lit";
import { repeat } from "lit/directives/repeat.js";
import { QueryStateController } from "../../../controllers/index.js";
import * as queries from "../../../queries/index.js";
import { ComponentBase } from "../../base.js";

export class ClientListComponent extends ComponentBase {

    private clientQuery = new QueryStateController(
        this,
        () => this.context.clientQuery,
        () => [] as const,
    )

    render() {
        const clientState = this.clientQuery.state;

        if (!clientState) return html`<p>Loading clients, please wait...</p>`;

        const list = queries.selectClientList(clientState);
        if (list.length === 0) return html`
<p>
    You have no clients yet! Please create one first!
</p>
<app-action-bar>
    <app-dialog-action dialogName="client-create"><app-icon type="create"></app-icon> Create client</app-dialog-action>
</app-action-bar>
`;

        return html`
<app-action-bar>
    <app-dialog-action dialogName="client-create"><app-icon type="create"></app-icon> Create client</app-dialog-action>
</app-action-bar>

<app-scrollable>

<table>
<thead>
    <tr>
        <th></th>
        <th></th>
        <th>Name</th>
        <th>Created</th>
        <th></th>
    </tr>
</thead>

<tbody>
${repeat(list, item => item.client, item => html`
<tr>
    <td>
    <app-action-bar inline>
        <app-route-action routeName="client" .routeParameters=${{ client: item.client }}>
            <app-icon type="launch"></app-icon>
        </app-route-action>
    </app-action-bar>
    </td>
    <td>${item.client}</td>
    <td>${item.name}</td>
    <td>${item.created}</td>
    <td>
    <app-action-bar inline>
        <app-dialog-action dialogName="client-update" .dialogParameters=${{ client: item.client }}>
            <app-icon type="edit"></app-icon>
        </app-dialog-action>
        <app-dialog-action dialogName="client-delete" .dialogParameters=${{ client: item.client }}>
            <app-icon type="delete"></app-icon>
        </app-dialog-action>
    </app-action-bar>
    </td>
</tr>
`)}
</tbody>
</table>

</app-scrollable>


`;

    }

}

