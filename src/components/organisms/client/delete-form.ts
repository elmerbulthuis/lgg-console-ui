import assert from "assert";
import { html, nothing } from "lit";
import { property } from "lit/decorators.js";
import { BusyController, QueryStateController } from "../../../controllers/index.js";
import * as queries from "../../../queries/index.js";
import { ComponentBase } from "../../base.js";

export class ClientDeleteFormComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    private busy = new BusyController(this);

    private clientQuery = new QueryStateController(
        this,
        () => this.context.clientQuery,
        () => [] as const,
    )

    render() {
        const { isBusy } = this.busy;

        const { client } = this;
        assert(client != null, "client not set");

        const { state } = this.clientQuery;
        if (state == null) return nothing;

        const item = queries.selectClientItem(state, client);
        if (item == null) return nothing;

        return html`
<p>
    Delete client "${item.name}"?
</p>

<app-action-bar>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleSubmit}>
    <app-icon type="thumb_up"></app-icon>
    Ok
</app-clickable-action>
<app-clickable-action ?disabled=${isBusy} @action=${this.handleCancel}>
    <app-icon type="undo"></app-icon>
    Cancel
</app-clickable-action>
</app-action-bar>
`;
    }

    private handleSubmit = this.busy.wrap(async () => {
        const { client } = this;
        assert(client, "missing client");

        const result = await this.context.consoleApi.deleteClient({
            parameters: {
                client,
            },
        });

        assert(result.status === 204);

        this.context.navigator.back();
    })

    private handleCancel = async (event: Event) => {
        this.context.navigator.back();
    }

}

