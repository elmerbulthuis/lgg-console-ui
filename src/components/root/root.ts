import assert from "assert";
import { css, html, nothing } from "lit";
import { property, state } from "lit/decorators.js";
import { Context, createContext } from "../../application/context.js";
import { ComponentBase } from "../base.js";

export class RootComponent extends ComponentBase {
    static globalStyles = css`
@font-face {
    font-family: 'Material Icons';
    font-style: normal;
    font-weight: 400;
    src: url(https://fonts.gstatic.com/s/materialicons/v118/flUhRq6tzZclQEJ-Vdg-IuiaDsNZ.ttf) format('truetype');
}

body {
    all: initial;
    background-color: var(--background-color);
}
`;

    static styles = [css`
nav {
    display: flex;
    flex-direction: column;
    align-items: left;
    gap: var(--padding);
    padding: var(--padding);
}
`];

    globalStyleElement?: HTMLStyleElement;

    @state()
    routeElement?: HTMLElement;

    @state()
    dialogElement?: HTMLElement;

    @property({
        attribute: "lgg-console-api-endpoint",
        reflect: true,
        converter: {
            toAttribute: (value: URL) => value == null ? undefined : value.toString(),
            fromAttribute: value => value == null ? undefined : new URL(value),
        },
    })
    lggConsoleApiEndpoint?: URL

    @property({
        attribute: "oauth-client-id",
        type: String,
        reflect: true,
    })
    oauthClientId?: string

    @property({
        attribute: "oauth-authorization-endpoint",
        reflect: true,
        converter: {
            toAttribute: (value: URL) => value == null ? undefined : value.toString(),
            fromAttribute: value => value == null ? undefined : new URL(value),
        },
    })
    oauthAuthorizationEndpoint?: URL

    @property({
        attribute: "oauth-token-endpoint",
        reflect: true,
        converter: {
            toAttribute: (value: URL) => value == null ? undefined : value.toString(),
            fromAttribute: value => value == null ? undefined : new URL(value),
        },
    })
    oauthTokenEndpoint?: URL

    @property({
        attribute: "retry-interval-base",
        type: Number,
        reflect: true,
    })
    retryIntervalBase?: number

    @property({
        attribute: "retry-interval-cap",
        type: Number,
        reflect: true,
    })
    retryIntervalCap?: number

    @property({
        attribute: "linger",
        type: Number,
        reflect: true,
    })
    linger?: number

    getRouteElement() {
        const info = this.context.navigator.getRoute();
        if (info == null) return;

        const element = this.ownerDocument.createElement("app-" + info.name + "-route");
        Object.entries(info.parameters).
            forEach(([key, value]) => element.setAttribute(key, value));

        return element;
    }

    getDialogElement() {
        const info = this.context.navigator.getDialog();
        if (info == null) return;

        const element = this.ownerDocument.createElement("app-" + info.name + "-dialog");
        Object.entries(info.parameters).
            forEach(([key, value]) => element.setAttribute(key, value));

        return element;
    }

    render() {
        const { routeElement, dialogElement } = this;

        if (this.context == null) {
            return nothing;
        }

        return html`
<app-layout ?dialog=${dialogElement != null}>
    <div slot="side">
        <nav>
        <app-route-action routeName="home"><app-icon type="home"></app-icon> Home</app-route-action>
        <app-route-action routeName="login"><app-icon type="login"></app-icon> Login</app-route-action>
        </nav>
    </div>


    <div>
        ${routeElement}
    </div>
    
    <div slot="dialog">
        ${dialogElement == null ? nothing : html`<app-dialog>${dialogElement}</app-dialog>`}
    </div>
</app-layout>
`;
    }

    connectedCallback() {
        super.connectedCallback();

        const view = this.ownerDocument.defaultView;
        assert(view);

        assert(this.lggConsoleApiEndpoint != null);
        assert(this.oauthClientId != null);
        assert(this.oauthAuthorizationEndpoint != null);
        assert(this.oauthTokenEndpoint != null);
        assert(this.retryIntervalBase != null);
        assert(this.retryIntervalCap != null);

        const onError = (error: unknown) => console.warn(error);

        this.maybeContext = createContext(
            this.ownerDocument,
            this.lggConsoleApiEndpoint,
            this.oauthClientId,
            this.oauthAuthorizationEndpoint,
            this.oauthTokenEndpoint,
            this.retryIntervalBase,
            this.retryIntervalCap,
            onError,
        );

        view.addEventListener("navigate", this.handleNavigate);
        view.addEventListener("popstate", this.handlePopState);
        view.addEventListener("keyup", this.handleKeyUp);

        this.routeElement = this.getRouteElement();

        this.globalStyleElement = this.ownerDocument.createElement("style");
        this.globalStyleElement.innerHTML = RootComponent.globalStyles.toString();
        this.ownerDocument.head.appendChild(this.globalStyleElement);
    }

    disconnectedCallback() {
        super.disconnectedCallback();

        const view = this.ownerDocument.defaultView;
        assert(view);

        this.maybeContext = undefined;

        view.removeEventListener("navigate", this.handleNavigate);
        view.removeEventListener("popstate", this.handlePopState);
        view.removeEventListener("keyup", this.handleKeyUp);

        this.routeElement = undefined;

        assert(this.globalStyleElement != null);
        this.ownerDocument.head.removeChild(this.globalStyleElement);
    }

    private handlePopState = () => {
        this.routeElement = this.getRouteElement();
        this.dialogElement = this.getDialogElement();
    }

    private handleNavigate = () => {
        this.routeElement = this.getRouteElement();
        this.dialogElement = this.getDialogElement();
    }

    private handleKeyUp = (event: KeyboardEvent) => {
        if (this.dialogElement == null) {
            return;
        }

        if (event.key !== "Escape") {
            return;
        }

        this.context.navigator.back();
    }

    private maybeContext?: Context
    get context(): Context {
        assert(this.maybeContext, "no context");
        return this.maybeContext;
    }
    set context(value: Context) {
        this.maybeContext = value;
    }

    getRootComponent() {
        return this;
    }

}
