import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-action-bar": index.ActionBarComponent;
        "app-scrollable": index.ScrollableComponent;
    }

}

customElements.define("app-action-bar", index.ActionBarComponent);
customElements.define("app-scrollable", index.ScrollableComponent);

