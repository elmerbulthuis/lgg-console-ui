import { css, html } from "lit";
import { property } from "lit/decorators.js";
import { ComponentBase } from "../../base.js";

export abstract class ActionComponentBase extends ComponentBase {
    @property({ type: Boolean, reflect: true })
    disabled = false

    static styles = [css`
:host {
    cursor: pointer;
    box-sizing: border-box;
    color: var(--dark-color);
    font-weight: normal;
}
:host(:hover) {
    font-weight: bold;
}
:host([disabled]) {
    cursor: not-allowed;
    opacity: 0.5;
}
:host([disabled]:hover) {
    font-weight: normal;
}

`];

    render() {
        return html`<slot></slot>`;
    }

    connectedCallback() {
        super.connectedCallback();

        this.renderRoot.addEventListener("click", this.handleClick);
    }

    disconnectedCallback() {
        super.disconnectedCallback();

        this.renderRoot.removeEventListener("click", this.handleClick);
    }

    private handleClick = (event: Event) => {
        if (this.disabled) {
            return;
        }

        const executeDefault = this.dispatchActionEvent();
        if (!executeDefault) {
            event.preventDefault();
        }
    }

    protected dispatchActionEvent() {
        const event = new Event(
            "action",
            {
                bubbles: true,
                composed: true,
                cancelable: true,
            },
        );
        const executeDefault = this.dispatchEvent(event);
        return executeDefault;
    }
}
