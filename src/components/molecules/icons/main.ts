import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-icon": index.IconComponent;
    }

}

customElements.define("app-icon", index.IconComponent);

