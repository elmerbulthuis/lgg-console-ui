import { property } from "@lit/reactive-element/decorators/property.js";
import { css } from "lit";
import { ComponentBase } from "../../base.js";

export class IconComponent extends ComponentBase {

    static styles = [css`
:host {
    all: initial;
    font-size: inherit;
    color: inherit;
    text-transform: none;
    font-family: 'Material Icons';
    cursor: inherit;
}
`];

    @property({ type: String, reflect: true })
    type?: string

    render() {
        return this.type;
    }
}
