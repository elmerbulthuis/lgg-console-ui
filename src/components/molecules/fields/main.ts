import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-text-field": index.TextField;
        "app-number-field": index.NumberField;
    }

}

customElements.define("app-text-field", index.TextField);
customElements.define("app-number-field", index.NumberField);

