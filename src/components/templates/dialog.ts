import { css, html } from "lit";
import { ComponentBase } from "../base.js";

export class DialogComponent extends ComponentBase {
    static styles = [...super.styles, css`
:host {
    display: block;
    position: fixed;
    background-color: var(--background-color);
    border: 1px solid var(--border-color);
    box-sizing: border-box;
    overflow: auto;
    margin: 0 auto;
    left: 0;
    right: 0;
    width: var(--dialog-width);
    top: var(--padding);
    bottom: var(--padding);
}

`];

    render() {
        return html`<slot></slot>`;
    }
}
