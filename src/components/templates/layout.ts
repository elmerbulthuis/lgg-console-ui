import { css, html, nothing } from "lit";
import { property } from "lit/decorators.js";
import { ComponentBase } from "../base.js";

export class LayoutComponent extends ComponentBase {
    static styles = [...super.styles, css`
aside {
    box-sizing: border-box;
    display: block;
    position: fixed;
    left: 0;
    top: 0;
    bottom: 0;
    width: var(--side-menu-width);
    border-right: solid 1px var(--border-color);
}

main {
    display: block;
    padding-left: var(--side-menu-width);;
}

#overlay {
    display: none;
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-color: var(--background-color);
    opacity: 0;
}

:host([dialog]) #overlay {
    display: block;
    opacity: 0.5;
    transition: opacity 1s;
}

`];

    @property({ type: Boolean, reflect: true })
    dialog = false;

    render() {
        return html`
<aside>
    <slot name="side"></slot>
</aside>
<main>
    <slot></slot>
</main>

<div id="overlay"></div>

${this.dialog ? html`<slot name="dialog"></slot>` : nothing}

`;

    }

}
