import assert from "assert";
import { html, nothing } from "lit";
import { property } from "lit/decorators.js";
import { QueryStateController } from "../../controllers/index.js";
import * as queries from "../../queries/index.js";
import { ComponentBase } from "../base.js";

export class ClientRouteComponent extends ComponentBase {
    @property({ type: String, reflect: true })
    client?: string

    private clientQuery = new QueryStateController(
        this,
        () => this.context.clientQuery,
        () => [] as const,
    )

    render() {
        const { client } = this;
        assert(client != null, "client not set");

        const { state } = this.clientQuery;
        if (state == null) return nothing;

        const item = queries.selectClientItem(state, client);
        if (item == null) return nothing;

        return html`
<h1>
Client "${item.name}"
</h1>

<app-client-secret-list client=${client}></app-client-secret-list>
`;
    }

}

