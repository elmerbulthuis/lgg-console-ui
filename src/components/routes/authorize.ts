import { html, PropertyValues } from "lit";
import { state } from "lit/decorators.js";
import { ComponentBase } from "../base.js";

export class AuthorizeRouteComponent extends ComponentBase {
    @state()
    error?: Error;

    render() {

        if (this.error == null) {

            return html`
<h1>Authorizing</h1>
<p>
    Please wait...
</p>
`;
        }
        else {
            return html`
<h1>Authorizing</h1>
<p>
    ${this.error.message}
</p>
`;
        }
    }

    firstUpdated(changedProperties: PropertyValues): void {
        super.firstUpdated(changedProperties);

        this.context.authorizer.authorize().catch(error => this.error = error);
    }

}

