import { html } from "lit";
import { ComponentBase } from "../base.js";

export class LoginRouteComponent extends ComponentBase {
    render() {
        return html`
<h1>Login</h1>

<p>
    You should login
</p>

<p>
    <app-clickable-action @click=${this.loginClick}>Login</app-clickable-action>
</p>
`;
    }

    loginClick = () => {
        this.context.authorizer.login();
    }

}

