import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-home-route": index.HomeRouteComponent;
        "app-login-route": index.LoginRouteComponent;
        "app-authorize-route": index.AuthorizeRouteComponent;
        "app-client-route": index.ClientRouteComponent;
    }
}

customElements.define("app-home-route", index.HomeRouteComponent);
customElements.define("app-login-route", index.LoginRouteComponent);
customElements.define("app-authorize-route", index.AuthorizeRouteComponent);
customElements.define("app-client-route", index.ClientRouteComponent);

