import "./dialogs/main.js";
import "./molecules/main.js";
import "./organisms/main.js";
import "./root/main.js";
import "./routes/main.js";
import "./templates/main.js";

