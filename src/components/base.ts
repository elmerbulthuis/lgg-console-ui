import { css, LitElement } from "lit";
import { state } from "lit/decorators.js";
import { Context } from "../application/context.js";
import { RootComponent } from "./root/index.js";

export abstract class ComponentBase extends LitElement {
    static styles = [css`

:host {
    font-family: sans-serif;
    font-size: var(--font-size);
}

h1,
h2,
h3,
p {
    all: initial;
    font: inherit;
    display: block;
    padding: var(--padding);
}
h1,
h2,
h3 {
    font-family:  sans-serif;
    color: var(--brand-brown);
}
p {
    color: var(--dark-color);
}
h1 {
    font-size: 2em;
}
h2 {
    font-size: 1.5em;
}
h3 {
    font-size: 1.2em;
}

p {
    font-size: 1em;
}

pre {
    font-family: monospace;
    font-size: var(--font-size);
    padding: var(--padding);
}

fieldset {
    all: initial;
    font: inherit;
    display: flex;
    flex-direction: column;
    gap: var(--padding);
    padding: var(--padding);
}

`];

    @state()
    get context(): Context {
        return this.getRootComponent().context;
    }

    getRootComponent(): RootComponent {
        const rootNode = this.getRootNode();
        if (rootNode instanceof ShadowRoot) {
            if (rootNode.host instanceof ComponentBase) {
                return rootNode.host.getRootComponent();
            }
        }

        throw new Error("not found");
    }

}
