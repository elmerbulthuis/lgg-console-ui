import fetch from "node-fetch";

export async function getOpenIdConfiguration(oauthIssuer: URL) {
    const openIdConfigurationUrl = new URL("/.well-known/openid-configuration", oauthIssuer);

    const openIdConfigurationResult = await fetch(openIdConfigurationUrl.toString());
    const openIdConfiguration = await openIdConfigurationResult.json() as any;
    return openIdConfiguration;
}
