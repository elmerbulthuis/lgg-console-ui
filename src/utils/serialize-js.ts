export function serializeJs(value: unknown): string {
    switch (typeof value) {
        case "object": {
            if (value === null) {
                break;
            }

            if (Array.isArray(value)) {
                return "[" +
                    value.map(serializeJs).
                        join(",") +
                    "[";
            }

            if (value instanceof URL) {
                return "new URL(" +
                    serializeJs(value.toString()) +
                    ")";
            }

            return "{" +
                Object.entries(value).
                    map(kv => kv.map(serializeJs).join(":")).
                    join(",") +
                "}";
        }

        case "undefined":
            return "undefined";
    }

    return JSON.stringify(value);
}
