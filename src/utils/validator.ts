export type FieldValidators<Model> =
    Record<keyof Model, (model: unknown) => Iterable<string[]>>;
