import test from "tape-promise/tape.js";
import { encodeHtml } from "./html.js";

test("encode-html", async t => {
    const actual = encodeHtml("\"\"");
    const expected = "&quot;&quot;";

    t.equal(actual, expected);
});
