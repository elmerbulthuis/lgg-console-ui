import base64 from "@hexagon/base64";

export function decodeJWT(token: string) {
    const textDecoder = new TextDecoder();

    const parts = token.split(".");

    const headerBuffer = base64.toArrayBuffer(parts[0], true);
    const payloadBuffer = base64.toArrayBuffer(parts[1], true);

    const headerString = textDecoder.decode(headerBuffer);
    const payloadString = textDecoder.decode(payloadBuffer);

    const header = JSON.parse(headerString);
    const payload = JSON.parse(payloadString);

    return {
        header, payload,
    };
}

export function encodeJWT(header: object, payload: object) {
    const textEncoder = new TextEncoder();

    const headerString = JSON.stringify(header);
    const payloadString = JSON.stringify(payload);

    const headerBuffer = textEncoder.encode(headerString);
    const payloadBuffer = textEncoder.encode(payloadString);

    const parts = [
        base64.fromArrayBuffer(headerBuffer),
        base64.fromArrayBuffer(payloadBuffer),
        "",
    ];

    const token = parts.join(".");

    return token;
}

