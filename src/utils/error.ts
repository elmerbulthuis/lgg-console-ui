export function isErrorEvent<T>(
    event: ErrorEvent | T,
): event is ErrorEvent {
    return "error" in event && event.error;
}

export interface ErrorEvent {
    error: true;
}
