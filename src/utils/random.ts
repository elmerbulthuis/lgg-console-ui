import * as random from "random-js";

export const randomString = random.
    string("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").
    bind(null, random.browserCrypto);

