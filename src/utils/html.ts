export function encodeHtml(value: string) {
    return value
        .replace(/&/g, "&amp;")
        .replace(/"/g, "&quot;")
        .replace(/>/g, "&gt;")
        .replace(/</g, "&lt;");
}

export function encodeHtmlAttribute(name: string, value: unknown) {
    if (value == null) {
        return "";
    }

    if (typeof value === "string") {
        return `${name}="${encodeHtml(value)}"`;
    }

    if (typeof value === "number") {
        return `${name}="${String(value)}"`;
    }

    if (typeof value === "boolean") {
        if (value) {
            return name;
        }
        else {
            return "";
        }
    }

    if (value instanceof URL) {
        return `${name}="${value.toString()}"`;
    }

    if (typeof value === "object") {
        return `${name}="${encodeHtml(JSON.stringify(value))}"`;
    }

    throw new Error("value not supported");
}

export function encodeHtmlAttributeMap(map: Record<string, unknown>) {
    return Object.entries(map).
        map(([key, value]) => encodeHtmlAttribute(key, value)).
        filter(value => value != "").
        join(" ");
}
