export function isIterableEmpty(iterable: Iterable<unknown>) {
    for (const element of iterable) {
        return false;
    }
    return true;
}
