import * as selenium from "selenium-webdriver";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

if (process.env.BROWSER) test("ui client", t => withContext(async context => {
    const uiClient = await context.createBrowser();
    await uiClient.get(new URL("/", context.endpoints.consoleUi).toString());

    const elements = await uiClient.findElements(selenium.By.css("app-root"));
    t.equal(elements.length, 1);
}));
