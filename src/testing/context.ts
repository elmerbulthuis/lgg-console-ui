import * as consoleOas from "@latency.gg/lgg-console-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import assert from "assert";
import * as http from "http";
import Koa from "koa";
import koaStatic from "koa-static";
import { minute, second } from "msecs";
import * as path from "path";
import * as selenium from "selenium-webdriver";
import * as seleniumChrome from "selenium-webdriver/chrome.js";
import * as seleniumEdge from "selenium-webdriver/edge.js";
import * as seleniumFirefox from "selenium-webdriver/firefox.js";
import { Promisable } from "type-fest";
import { renderMainHtml } from "../application/render.js";
import { createRouter } from "../application/router.js";
import { Settings } from "../application/settings.js";
import { projectRoot } from "../utils/root.js";
import { encodeJWT } from "../utils/token.js";

export type ContextBrowser =
    "firefox" | "firefox-headless" |
    "chrome" | "chrome-headless" |
    // safari does not have a headless mode (https://discussions.apple.com/thread/251837694)
    "safari" |
    "edge" | "edge-headless";
export const contextBrowser = process.env.BROWSER as ContextBrowser | undefined;

interface Context {
    endpoints: {
        consoleUi: URL,
        consoleApi: URL,
        consoleOauth: URL,
    },
    servers: {
        consoleUi: Koa,
        consoleApi: consoleOas.Server,
        consoleOauth: Koa,
    },
    createBrowser(): Promise<selenium.WebDriver>,
}

export async function withContext(
    job: (context: Context) => Promisable<void>,
    onError?: (error: unknown) => void,
) {
    const endpoints = {
        consoleUi: new URL("http://localhost:8080"),
        consoleApi: new URL("http://localhost:9100"),
        consoleOauth: new URL("http://localhost:9200"),
    };

    const applicationSettings: Settings = {
        lggConsoleApiEndpoint: endpoints.consoleApi,
        oauthIssuer: endpoints.consoleOauth,
        oauthClientId: "x",
        retryIntervalBase: 1 * second,
        retryIntervalCap: 1 * second,
        linger: 30 * second,
    };

    const router = createRouter();

    const consoleUi = new Koa();
    consoleUi.use(koaStatic(path.join(projectRoot, "assets")));
    consoleUi.use(koaStatic(path.join(projectRoot, "bundle")));
    consoleUi.use(async (context, next) => {
        const route = router.parseRoute(context.path);
        if (!route) return next();

        context.body = await renderMainHtml(applicationSettings);
    });

    const consoleApi = new consoleOas.Server({});
    consoleApi.registerMiddleware(oas3ts.createErrorMiddleware(onError));
    consoleApi.registerMiddleware(
        oas3ts.createCorsMiddleware({
            allowOrigin: "*",
            maxAge: 15 * minute,
            metadata: consoleOas.metadata,
        }),
    );

    const consoleOauth = new Koa();
    let lastNonce = "";
    consoleOauth.use((context, next) => {
        context.set("access-control-allow-origin", endpoints.consoleUi.origin);
        if (context.method === "OPTIONS") {
            context.status = 204;
            return;
        }

        switch (context.path) {
            case "/.well-known/openid-configuration":
                context.body = {
                    authorization_endpoint: new URL("/authorization", endpoints.consoleOauth),
                    token_endpoint: new URL("/token", endpoints.consoleOauth),
                };
                break;

            case "/authorization": {
                assert(typeof context.query.redirect_uri === "string");
                assert(typeof context.query.state === "string");
                assert(typeof context.query.nonce === "string");

                const redirect = new URL(context.query.redirect_uri);
                redirect.searchParams.append("state", context.query.state);
                redirect.searchParams.append("code", "super-secret-code");

                lastNonce = context.query.nonce;

                context.redirect(redirect.toString());
                break;
            }

            case "/token":
                context.body = {
                    token_type: "bearer",
                    access_token: "super-secret-access-token",
                    id_token: encodeJWT(
                        {},
                        {
                            nonce: lastNonce,
                        },
                    ),
                };
                break;

            default:
                return next();
        }
    });

    const servers = {
        consoleUi,
        consoleApi,
        consoleOauth,
    };

    const httpServers = {
        consoleUi: http.createServer(servers.consoleUi.callback()),
        consoleApi: http.createServer(servers.consoleApi.asRequestListener({
            onError,
        })),
        consoleOauth: http.createServer(servers.consoleOauth.callback()),
    };

    const keys = Object.keys(endpoints) as Array<keyof typeof endpoints>;
    await Promise.all(
        keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
            endpoints[key].port,
            () => resolve(),
        ))),
    );
    try {
        const destructors = new Array<() => Promise<void>>();
        try {
            const context = {
                endpoints,
                servers,
                async createBrowser() {
                    assert(contextBrowser, "context browser not set");

                    let builder = new selenium.Builder();
                    switch (contextBrowser) {
                        case "firefox":
                        case "chrome":
                        case "safari":
                        case "edge":
                            builder = builder.forBrowser(contextBrowser);
                            break;

                        case "firefox-headless":
                            builder = builder.forBrowser("firefox");
                            builder = builder.setFirefoxOptions(
                                new seleniumFirefox.Options().headless(),
                            );
                            break;

                        case "chrome-headless":
                            builder = builder.forBrowser("chrome");
                            builder = builder.setChromeOptions(
                                new seleniumChrome.Options().headless(),
                            );
                            break;

                        case "edge-headless":
                            builder = builder.forBrowser("edge");
                            builder = builder.setEdgeOptions(
                                new seleniumEdge.Options().headless(),
                            );
                            break;

                        default: assert.fail(`unknown context browser ${contextBrowser}`);
                    }

                    const driver = await builder.build();
                    destructors.push(() => driver.quit());

                    return driver;
                },
            };

            await job(context);
        }
        finally {
            await Promise.all(destructors.map(destructor => destructor()));
        }
    }
    finally {
        await Promise.all(keys.map(
            async key => new Promise<void>((resolve, reject) => httpServers[key].close(
                error => error ?
                    reject(error) :
                    resolve(),
            )),
        ));
    }
}
