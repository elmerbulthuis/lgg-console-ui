import base64 from "@hexagon/base64";
import assert from "assert";
import { randomString } from "../utils/random.js";
import { decodeJWT } from "../utils/token.js";
import { Navigator } from "./navigator.js";

export interface AuthorizationState {
    authorizationCookie: string,
    path: string,
}

interface TokenResponse {
    token_type: string
    expires_in: number
    access_token: string
    id_token: string
    refresh_token: string
}

export class Authorizer {
    constructor(
        private readonly navigator: Navigator,
        private readonly document: Document,
        private readonly oauthClientId: string,
        private readonly authorizationEndpoint: URL,
        private readonly tokenEndpoint: URL,
    ) {

    }

    public getAccessToken() {
        const view = this.document.defaultView;
        assert(view != null);

        return view.sessionStorage.getItem("access-token");
    }

    public getIdToken() {
        const view = this.document.defaultView;
        assert(view != null);

        return view.sessionStorage.getItem("id-token");
    }

    async login() {
        const baseUrl = new URL("/", this.document.location.href);

        const view = this.document.defaultView;
        assert(view != null);

        const route = this.navigator.getRoute();
        assert(route);

        const textEncoder = new TextEncoder();

        const nonce = randomString(64);
        const verifier = randomString(64);
        const verifierBuffer = textEncoder.encode(verifier);
        const challengeBuffer = await crypto.subtle.digest("SHA-256", verifierBuffer);
        const challenge = base64.fromArrayBuffer(challengeBuffer, true);

        const authorizationCookie = randomString(64);
        view.sessionStorage.setItem("authorization-cookie", authorizationCookie);
        view.sessionStorage.setItem("verifier", verifier);
        view.sessionStorage.setItem("nonce", nonce);

        const state: AuthorizationState = {
            authorizationCookie,
            path: this.document.location.pathname,
        };

        const redirectUri = new URL(
            this.navigator.stringifyRoute("authorize"),
            baseUrl,
        );

        const url = new URL("", this.authorizationEndpoint);
        url.searchParams.append("client_id", this.oauthClientId);
        url.searchParams.append("redirect_uri", redirectUri.toString());
        url.searchParams.append("response_type", "code");
        url.searchParams.append("response_mode", "query");
        url.searchParams.append("scope", "openid");
        url.searchParams.append("state", JSON.stringify(state));
        url.searchParams.append("nonce", nonce);
        url.searchParams.append("code_challenge", challenge);
        url.searchParams.append("code_challenge_method", "S256");

        this.navigator.replaceLocation(url);

        await new Promise(resolve => {
            //
        });
    }

    async authorize() {
        const baseUrl = new URL("/", this.document.location.href);

        const view = this.document.defaultView;
        assert(view != null);

        const verifier = view.sessionStorage.getItem("verifier");
        const authorizationCookie = view.sessionStorage.getItem("authorization-cookie");
        const nonce = view.sessionStorage.getItem("nonce");

        view.sessionStorage.removeItem("access-token");
        view.sessionStorage.removeItem("id-token");
        view.sessionStorage.removeItem("verifier");
        view.sessionStorage.removeItem("authorization-cookie");
        view.sessionStorage.removeItem("nonce");

        const params = this.navigator.getSearchParams();
        assert(params != null, "expected search");

        const error = params.get("error");
        const code = params.get("code");
        const state = JSON.parse(params.get("state") ?? "{}") as AuthorizationState;

        assert(error == null, `error: ${error}`);
        assert(code != null, "expected code");

        assert(state.authorizationCookie === authorizationCookie, "authorizationCookie mismatch");
        assert(verifier != null, "missing verifier");

        const tokenResponse = await this.exchangeCode(code, verifier);
        assert(tokenResponse.token_type.toLowerCase() === "bearer", "expected bearer token type");

        view.sessionStorage.setItem("access-token", tokenResponse.access_token);
        view.sessionStorage.setItem("id-token", tokenResponse.id_token);

        const idTokenDecoded = decodeJWT(tokenResponse.id_token);
        assert(idTokenDecoded.payload.nonce === nonce, "nonce mismatch");

        this.navigator.replaceLocation(
            new URL(
                state.path,
                baseUrl,
            ),
        );
    }

    private async exchangeCode(
        code: string,
        verifier: string,
    ): Promise<TokenResponse> {
        const baseUrl = new URL("/", this.document.location.href);

        assert(this.oauthClientId);

        const redirectUri = new URL(
            this.navigator.stringifyRoute("authorize"),
            baseUrl,
        );

        const url = new URL("", this.tokenEndpoint);
        const requestBody = new URLSearchParams();
        requestBody.append("grant_type", "authorization_code");
        requestBody.append("code", code);
        requestBody.append("redirect_uri", redirectUri.toString());
        requestBody.append("client_id", this.oauthClientId);
        requestBody.append("code_verifier", verifier);

        const response = await fetch(
            url.toString(),
            {
                method: "POST",
                body: requestBody,
            },
        );
        assert(response.status === 200, "unexpected status");

        const responseBody = await response.json();

        return responseBody;
    }

}

