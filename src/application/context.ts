import * as consoleOas from "@latency.gg/lgg-console-oas";
import * as queries from "../queries/index.js";
import { Authorizer } from "./authorizer.js";
import { Navigator } from "./navigator.js";
import { createRouter } from "./router.js";

export type Context = ReturnType<typeof createContext>;

export function createContext(
    document: Document,
    lggConsoleApiEndpoint: URL,
    oauthClientId: string,
    oauthAuthorizationEndpoint: URL,
    oauthTokenEndpoint: URL,
    retryIntervalBase: number,
    retryIntervalCap: number,
    onError: (error: unknown) => void,
) {
    const router = createRouter();
    const navigator = new Navigator(router, document);
    const authorizer = new Authorizer(
        navigator,
        document,
        oauthClientId,
        oauthAuthorizationEndpoint,
        oauthTokenEndpoint,
    );

    const consoleApi = new consoleOas.Client(
        {
            baseUrl: lggConsoleApiEndpoint,
        },
        {
            accessToken: authorizer.getIdToken() ?? undefined,
        },
    );

    consoleApi.registerMiddleware(
        async (request, next) => {
            const response = await next(request);

            switch (response.status) {
                case 401: // Unauthorized
                    await authorizer.login();
                    return response;

                default: return response;
            }
        },
    );

    const clientQuery = queries.createClientQueryFactory(
        retryIntervalBase,
        retryIntervalCap,
        { consoleApi },
        onError,
    );

    const clientSecretQuery = queries.createClientSecretQueryFactory(
        retryIntervalBase,
        retryIntervalCap,
        { consoleApi },
        onError,
    );

    return {
        router,
        navigator,
        authorizer,
        consoleApi,
        clientQuery,
        clientSecretQuery,
    };
}

