import { Router } from "goodrouter";

export function createRouter() {
    const router = new Router();

    router.insertRoute("home", "/");
    router.insertRoute("login", "/login");
    router.insertRoute("authorize", "/authorize");
    router.insertRoute("client", "/client/{client}");

    return router;
}

