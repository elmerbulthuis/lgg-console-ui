import { encodeHtmlAttributeMap } from "../utils/html.js";
import { getOpenIdConfiguration } from "../utils/oidc.js";
import { Settings } from "./settings.js";

export async function renderMainHtml(applicationSettings: Settings) {
    const openIdConfiguration = await getOpenIdConfiguration(applicationSettings.oauthIssuer);

    const rootAttributes = encodeHtmlAttributeMap({
        "lgg-console-api-endpoint": applicationSettings.lggConsoleApiEndpoint,
        "oauth-authorization-endpoint": openIdConfiguration.authorization_endpoint,
        "oauth-token-endpoint": openIdConfiguration.token_endpoint,
        "oauth-client-id": applicationSettings.oauthClientId,
        "retry-interval-base": applicationSettings.retryIntervalBase,
        "retry-interval-cap": applicationSettings.retryIntervalCap,
        "linger": applicationSettings.linger,
    });

    return `<!DOCTYPE html>
    
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noindex,nofollow">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="theme-color" content="#5e2d25"/>

<style>

:root {
    --brand-blue: #accbc6;
    --brand-yellow: #e7b764;
    --brand-brown: #5e2d25;

    --font-size: 15px;
    --padding: 10px;

    --background-color: white;
    --border-color: var(--brand-yellow);
    --dark-color: black;

    --side-menu-width: 200px;
    --dialog-width: 900px;
}

</style>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Latency.GG console</title>

<script src="/main.js"></script>

</head>

<body>

<app-root ${rootAttributes}></app-root>

</body>

</html>
`;
}
