export interface Settings {
    lggConsoleApiEndpoint: URL;

    oauthIssuer: URL,
    oauthClientId: string,

    retryIntervalBase: number,
    retryIntervalCap: number,
    linger: number,
}
