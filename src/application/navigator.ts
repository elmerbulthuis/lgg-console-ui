import assert from "assert";
import { Router } from "goodrouter";

export interface NavigationInfo {
    name: string;
    parameters: Record<string, string>;
}

export class Navigator {
    constructor(
        private router: Router,
        private document: Document,
    ) {

    }

    dispatchNavigate() {
        const view = this.document.defaultView;
        assert(view != null);

        const event = new Event(
            "navigate",
            { bubbles: true, composed: true },
        );
        view.dispatchEvent(event);
    }

    stringifyRoute(name: string, parameters: Record<string, string> = {}) {
        const pathname = this.router.stringifyRoute({ name, parameters });
        assert(pathname);

        return pathname;
    }

    getRoute(): NavigationInfo | undefined {
        const { pathname } = this.document.location;

        const route = this.router.parseRoute(pathname);
        if (!route) return;

        const { name, parameters } = route;

        return {
            name, parameters,
        };
    }

    pushRoute(name: string, parameters: Record<string, string> = {}) {
        const view = this.document.defaultView;
        assert(view != null);

        const pathname = this.stringifyRoute(name, parameters);

        view.history.pushState(
            {
                dialog: undefined,
            },
            "",
            pathname,
        );

        this.dispatchNavigate();
    }

    replaceRoute(name: string, parameters: Record<string, string> = {}) {
        const view = this.document.defaultView;
        assert(view != null);

        const pathname = this.stringifyRoute(name, parameters);

        view.history.replaceState(
            {
                dialog: undefined,
            },
            "",
            pathname,
        );

        this.dispatchNavigate();
    }

    getDialog(): NavigationInfo | undefined {
        const view = this.document.defaultView;
        assert(view != null);

        const state = view.history.state;

        return state?.dialog;
    }

    pushDialog(name: string, parameters: Record<string, string> = {}) {
        const view = this.document.defaultView;
        assert(view != null);

        view.history.pushState(
            {
                dialog: {
                    name,
                    parameters,
                },
            },
            "",
        );

        this.dispatchNavigate();
    }

    replaceDialog(name: string, parameters: Record<string, string> = {}) {
        const view = this.document.defaultView;
        assert(view != null);

        view.history.replaceState(
            {
                dialog: {
                    name,
                    parameters,
                },
            },
            "",
        );

        this.dispatchNavigate();
    }

    pushLocation(url: URL) {
        this.document.location.href = url.toString();
    }

    replaceLocation(url: URL) {
        this.document.location.replace(url.toString());
    }

    getSearchParams() {
        const { search } = this.document.location;
        if (!search.startsWith("?")) return;

        const params = new URLSearchParams(this.document.location.search.substring(1));
        return params;
    }

    getHashParams() {
        const { hash } = this.document.location;
        if (!hash.startsWith("#")) return;

        const params = new URLSearchParams(this.document.location.hash.substring(1));
        return params;
    }

    back() {
        const view = this.document.defaultView;
        assert(view != null);

        view.history.back();
    }
}

