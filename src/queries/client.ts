import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import { Context } from "../application/context.js";
import { ErrorEvent, isErrorEvent } from "../utils/error.js";
import { createQueryFactory, QuerySource } from "../utils/query.js";

//#region query

export type ClientEventUnion =
    consoleOas.SubscribeClientEvents200ApplicationJsonResponseSchema;

export type ClientQuery = InstanceMemoizer<
    Promise<ClientQuerySource>, []
>

export type ClientQuerySource = QuerySource<ClientState, ClientEventUnion>

export function createClientQueryFactory(
    retryIntervalBase: number,
    retryIntervalCap: number,
    context: Pick<Context, "consoleApi">,
    onError: (error: unknown) => void,
): ClientQuery {
    return createQueryFactory({
        initialState, reduce,
        settle: event => true,
        createSource,
        calculateHash: () => "",
        onError,
        retryIntervalBase,
        retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
    ) {
        const source = await context.consoleApi.subscribeClientEvents({
            parameters: {},
        });
        assert(source.status === 200);
        yield* source.entities(signal);
    }
}

//#endregion

//#region state / events

export interface ClientEntity {
    name: string;
    created: string
}

export interface ClientState {
    error: boolean
    entities: immutable.Map<string, ClientEntity>;
}

const initialState: ClientState = {
    error: false,
    entities: immutable.Map(),
};

function reduce(
    state: ClientState,
    event: ClientEventUnion | ErrorEvent,
): ClientState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "client-snapshot": {
            let { entities } = initialState;

            entities = entities.withMutations(entities => {
                for (const client of event.payload.clients) {
                    const {
                        id,
                        name,
                        created,
                    } = client;

                    const entity = {
                        name,
                        created,
                    };
                    entities.set(id, entity);
                }
            });

            return {
                error: false,
                entities,
            };
        }

        case "client-created": {
            let { entities } = state;

            const {
                id,
                name,
                created,
            } = event.payload.client;
            const entity = {
                name,
                created,
            };

            assert(!entities.get(id));
            entities = entities.set(id, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-deleted": {
            let { entities } = state;

            const { id } = event.payload.client;

            assert(entities.has(id));

            entities = entities.delete(id);

            return {
                ...state,
                entities,
            };
        }

        case "client-updated": {
            let { entities } = state;

            const {
                id,
                name,
                created,
            } = event.payload.client;

            let entity = entities.get(id);
            assert(entity);
            entity = {
                ...entity,
                name,
                created,
            };

            entities = entities.set(id, entity);

            return {
                ...state,
                entities,
            };
        }
    }

}

//#endregion

//#region selectors

export function selectClientList(state: ClientState) {
    return [
        ...state.entities.
            map((value, key) => mapEntity(key, value)).
            sortBy(value => value.created).
            reverse().
            values(),
    ];
}

export function selectClientOptions(state: ClientState) {
    return selectClientList(state).
        map(({ client, name, created }) => [client, name] as const);
}

export function selectClientItem(
    state: ClientState,
    client: string,
) {
    const entity = state.entities.get(client);
    if (!entity) return;

    return mapEntity(client, entity);
}

export function selectClientName(state: ClientState, client: string) {
    return selectClientItem(state, client)?.name;
}

function mapEntity(client: string, entity: ClientEntity) {
    return {
        client,
        ...entity,
    };
}

//#endregion
