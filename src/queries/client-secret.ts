import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import { Context } from "../application/context.js";
import { ErrorEvent, isErrorEvent } from "../utils/error.js";
import { createQueryFactory, QuerySource } from "../utils/query.js";

//#region query

export type ClientSecretEventUnion =
    consoleOas.SubscribeClientSecretEvents200ApplicationJsonResponseSchema;

export type ClientSecretQuery = InstanceMemoizer<
    Promise<ClientSecretQuerySource>, [string]
>

export type ClientSecretQuerySource = QuerySource<ClientSecretState, ClientSecretEventUnion>

export function createClientSecretQueryFactory(
    retryIntervalBase: number,
    retryIntervalCap: number,
    context: Pick<Context, "consoleApi">,
    onError: (error: unknown) => void,
): ClientSecretQuery {
    return createQueryFactory({
        initialState, reduce,
        settle: event => true,
        createSource,
        calculateHash: client => client,
        onError,
        retryIntervalBase,
        retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
        client: string,
    ) {
        const source = await context.consoleApi.subscribeClientSecretEvents({
            parameters: {
                client,
            },
        });
        assert(source.status === 200);
        yield* source.entities(signal);
    }
}

//#endregion

//#region state / events

export interface ClientSecretEntity {
    created: string;
}

export interface ClientSecretState {
    error: boolean
    entities: immutable.Map<string, ClientSecretEntity>;
}

const initialState: ClientSecretState = {
    error: false,
    entities: immutable.Map(),
};

function reduce(
    state: ClientSecretState,
    event: ClientSecretEventUnion | ErrorEvent,
): ClientSecretState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "client-secret-snapshot": {
            let { entities } = initialState;

            entities = entities.withMutations(entities => {
                for (const clientSecret of event.payload.clientSecrets) {
                    const {
                        digest,
                        created,
                    } = clientSecret;

                    const entity = {
                        created,
                    };
                    entities.set(digest, entity);
                }
            });

            return {
                error: false,
                entities,
            };
        }

        case "client-secret-created": {
            let { entities } = state;

            const {
                digest,
                created,
            } = event.payload.clientSecret;
            const entity = {
                created,
            };

            assert(!entities.get(digest));
            entities = entities.set(digest, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-secret-deleted": {
            let { entities } = state;

            const { digest } = event.payload.clientSecret;

            assert(entities.has(digest));

            entities = entities.delete(digest);

            return {
                ...state,
                entities,
            };
        }

    }

}

//#endregion

//#region selectors

export function selectClientSecretList(state: ClientSecretState) {
    return [
        ...state.entities.
            map((value, key) => mapEntity(key, value)).
            sortBy(value => value.created).
            reverse().
            values(),
    ];
}

export function selectClientSecretOptions(state: ClientSecretState) {
    return selectClientSecretList(state).
        map(({ digest, created }) => [digest, created] as const);
}

export function selectClientSecretItem(
    state: ClientSecretState,
    digest: string,
) {
    const entity = state.entities.get(digest);
    if (!entity) return;

    return mapEntity(digest, entity);
}

function mapEntity(digest: string, entity: ClientSecretEntity) {
    return {
        digest,
        ...entity,
    };
}

//#endregion
