#!/usr/bin/env node

import { program } from "commander";
import "./programs/main.js";

program.parse(process.argv);
