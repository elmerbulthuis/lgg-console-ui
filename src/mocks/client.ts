import * as consoleOas from "@latency.gg/lgg-console-oas";
import { createBufferedIterable } from "buffered-iterable";
import * as crypto from "crypto";
import delay from "delay";
import { createIterableFanout } from "iterable-fanout";
import { second } from "msecs";
import { ClientEventUnion } from "../queries/index.js";

export function initializeClientMock(
    server: consoleOas.Server,
    lag = 1 * second,
) {
    const clientQuerySink = createBufferedIterable<
        ClientEventUnion
    >();
    const clientQueryFanout = createIterableFanout(clientQuerySink);

    server.registerSubscribeClientEventsOperation(incomingRequest => {
        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                await delay(lag, { signal });
                yield {
                    type: "client-snapshot",
                    payload: {
                        clients: [
                            {
                                id: "xx==",
                                name: "testing-123",
                                created: "2022-04-07T14:49:25Z",
                            },
                        ],
                    },
                };

                for await (const event of clientQueryFanout.fork(signal)) {
                    await delay(lag, { signal });
                    yield event;
                }
            },
        };
    });

    server.registerCreateClientOperation(async incomingRequest => {
        await delay(lag);

        const entity = await incomingRequest.entity();

        const {
            name,
        } = entity;

        const client = crypto.randomBytes(4).toString("base64");

        clientQuerySink.push({
            type: "client-created",
            payload: {
                client: {
                    id: client,
                    name,
                    created: new Date().toISOString(),
                },
            },
        });

        return {
            status: 201,
            parameters: {},
            entity() {
                return { id: client };
            },
        };
    });

    server.registerDeleteClientOperation(async incomingRequest => {
        await delay(lag);

        const { client } = incomingRequest.parameters;

        clientQuerySink.push({
            type: "client-deleted",
            payload: {
                client: {
                    id: client,
                    name: "",
                    created: new Date().toISOString(),
                },
            },
        });

        return {
            status: 204,
            parameters: {},
        };
    });

    server.registerUpdateClientOperation(async incomingRequest => {
        await delay(lag);

        const entity = await incomingRequest.entity();

        const { client } = incomingRequest.parameters;
        const {
            name,
        } = entity;

        clientQuerySink.push({
            type: "client-updated",
            payload: {
                client: {
                    id: client,
                    name,
                    created: new Date().toISOString(),
                },
            },
        });

        return {
            status: 204,
            parameters: {},
        };
    });

}
