import * as consoleOas from "@latency.gg/lgg-console-oas";
import { createBufferedIterable } from "buffered-iterable";
import * as crypto from "crypto";
import delay from "delay";
import { createIterableFanout } from "iterable-fanout";
import { second } from "msecs";
import { ClientSecretEventUnion } from "../queries/index.js";

export function initializeClientSecretMock(
    server: consoleOas.Server,
    lag = 1 * second,
) {
    const clientSecretQuerySink = createBufferedIterable<
        ClientSecretEventUnion
    >();
    const clientSecretQueryFanout = createIterableFanout(clientSecretQuerySink);

    server.registerSubscribeClientSecretEventsOperation(incomingRequest => {
        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                await delay(lag, { signal });
                yield {
                    type: "client-secret-snapshot",
                    payload: {
                        clientSecrets: [
                            {
                                digest: "xx",
                                created: "2022-04-07T16:33:20Z",
                            },
                        ],
                    },
                };

                for await (const event of clientSecretQueryFanout.fork(signal)) {
                    await delay(lag, { signal });
                    yield event;
                }
            },
        };
    });

    server.registerCreateClientSecretOperation(async incomingRequest => {
        await delay(lag);

        const digest = crypto.randomBytes(4).toString("base64");
        const secret = crypto.randomBytes(8).toString("base64");

        clientSecretQuerySink.push({
            type: "client-secret-created",
            payload: {
                clientSecret: {
                    digest,
                    created: new Date().toISOString(),
                },
            },
        });

        return {
            status: 201,
            parameters: {},
            entity() {
                return {
                    digest,
                    secret,
                };
            },
        };
    });

    server.registerDeleteClientSecretOperation(async incomingRequest => {
        await delay(lag);

        const { digest } = incomingRequest.parameters;

        clientSecretQuerySink.push({
            type: "client-secret-deleted",
            payload: {
                clientSecret: {
                    digest,
                    created: new Date().toISOString(),
                },
            },
        });

        return {
            status: 204,
            parameters: {},
        };
    });

}
