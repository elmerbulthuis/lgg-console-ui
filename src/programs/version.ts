import { program } from "commander";
import { packageInfo } from "../utils/package.js";

if (packageInfo.version) {
    program.version(packageInfo.version);
}
