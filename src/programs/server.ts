import { program } from "commander";
import * as http from "http";
import Koa from "koa";
import koaStatic from "koa-static";
import { minute, second } from "msecs";
import * as path from "path";
import { renderMainHtml } from "../application/render.js";
import { createRouter } from "../application/router.js";
import { Settings } from "../application/settings.js";
import { projectRoot } from "../utils/root.js";

program.
    command("server").
    option("--port <number>", "Listen to port", Number, 8080).
    requiredOption("--lgg-console-api-endpoint <string>", "", v => new URL(v)).
    requiredOption("--oauth-client-id <string>", "", String, process.env.OAUTH_CLIENT_ID).
    option("--oauth-issuer <string>", "", v => new URL(v), new URL(process.env.OAUTH_ISSUER ?? "https://gitlab.com")).
    option("--retry-interval-base <msec>", "", Number, 1 * second).
    option("--retry-interval-cap <msec>", "", Number, 1 * minute).
    option("--linger <msec>", "", Number, 30 * second).
    action(action);

interface ActionSettings {
    port: number;

    lggConsoleApiEndpoint: URL;

    oauthClientId: string;
    oauthIssuer: URL;

    retryIntervalBase: number,
    retryIntervalCap: number,
    linger: number,
}
async function action({
    port,

    lggConsoleApiEndpoint,

    oauthClientId,
    oauthIssuer,

    retryIntervalBase,
    retryIntervalCap,
    linger,
}: ActionSettings) {
    console.log("starting...");

    const applicationSettings: Settings = {
        lggConsoleApiEndpoint,

        oauthClientId,
        oauthIssuer,

        retryIntervalBase,
        retryIntervalCap,
        linger,
    };

    const server = new Koa();

    server.use(koaStatic(path.join(projectRoot, "assets")));
    server.use(koaStatic(path.join(projectRoot, "bundle")));

    const mainHtml = await renderMainHtml(applicationSettings);
    const router = createRouter();
    server.use((context, next) => {
        const route = router.parseRoute(context.path);
        if (!route) return next();

        context.body = mainHtml;
    });

    const httpServer = http.createServer(server.callback());

    await new Promise<void>(resolve => httpServer.listen(
        port,
        () => resolve(),
    ));
    try {
        console.log("started");

        await new Promise(resolve => process.addListener("SIGINT", resolve));

        console.log("stopping...");
    }
    finally {
        await new Promise<void>((resolve, reject) => httpServer.close(
            error => error ?
                reject(error) :
                resolve(),
        ));
    }

    console.log("stopped");
}
