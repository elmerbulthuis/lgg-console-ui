import { program } from "commander";
import * as mocks from "../mocks/index.js";
import { withContext } from "../testing/index.js";

program.
    command("test-context").
    action(action);

async function action() {
    console.log("starting...");

    await withContext(async context => {
        context.servers.consoleApi.registerAccessTokenAuthorization(credential => true);

        mocks.initializeClientMock(context.servers.consoleApi);
        mocks.initializeClientSecretMock(context.servers.consoleApi);

        console.log("started");

        await new Promise(resolve => process.addListener("SIGINT", resolve));

        console.log("stopping...");
    });

    console.log("stopped");
}
