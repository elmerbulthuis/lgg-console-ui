SHELL:=$(PREFIX)/bin/sh
PATH:=$(shell npm bin):$(PATH)
TS_SRC:=$(wildcard src/*.ts) $(wildcard src/*/*.ts) $(wildcard src/*/*/*.ts) $(wildcard src/*/*/*/*.ts)
TSX_SRC:=$(wildcard src/*.tsx) $(wildcard src/*/*.tsx) $(wildcard src/*/*/*.tsx) $(wildcard src/*/*/*/*.tsx)
JS_OUT:=$(patsubst src/%.ts,out/%.js,$(TS_SRC)) $(patsubst src/%.tsx,out/%.js,$(TSX_SRC))

VERSION?=v0.0.0-local
OAUTH_CLIENT_ID?=360134655249-7cp11qteb8uta7as2nhcd62httnek5ta.apps.googleusercontent.com
OAUTH_AUTHORIZATION_URL?=https://accounts.google.com/o/oauth2/v2/auth
ENDPOINT?=http://127.0.0.1:8080
ADMIN_BACK_ENDPOINT?=http://127.0.0.1:8081

rebuild: clean build

build: \
	pub/main.js \
	pub/main.js.map \
	pub/robots.txt \
	pub/favicon.ico \

clean:
	rm -rf out bundle pub

$(JS_OUT): tsconfig.json $(TS_SRC) $(TSX_SRC)
	tsc --project $<

bundle/main.js bundle/main.js.map: rollup.config.js $(JS_OUT)
	rollup --config $<

pub/main.js pub/main.js.map: bundle/main.js bundle/main.js.map
	@mkdir --parents $(@D)
	terser --mangle --compress passes=2 --source-map filename --output pub/main.js bundle/main.js

pub/%: assets/%
	@mkdir --parents $(@D)
	cp $< $@

.PHONY: \
	rebuild \
	build \
	clean \

