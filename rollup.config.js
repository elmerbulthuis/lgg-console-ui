import commonJS from "@rollup/plugin-commonjs";
import nodeResolve from "@rollup/plugin-node-resolve";
import nodePolyfills from "rollup-plugin-node-polyfills";
import sourcemaps from 'rollup-plugin-sourcemaps';

export default {
    input: "out/main.js",
    output: {
        file: "bundle/main.js",
        format: "iife",
        sourcemap: true,
    },
    plugins: [
        sourcemaps(),
        nodePolyfills(),
        nodeResolve({
            browser: true,
            mainFields: ["browser"],
        }),
        commonJS(),
    ],
};